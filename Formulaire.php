<!--- code  formulaire -->
<!--- Matéo Monteiro -->
<!--- BTS SIO 1 -->
<!--- TP classe pour Mr langloy -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <form action="enregistrement.php" method="post">
    <!--- lien pour page css -->
    <link rel="stylesheet" type="text/css" href="themeformulaire.css">
    <title>Contrat de partenariat commercial</title>
</head>
<body>
    <header>
        <h1>FORMULAIRE DE CONTRAT DE PARTENARIAT COMMERCIAL</h1>
    </header>

    <section>
        <p>Ce contrat est fait ce jour <input type="date" name="debut_du_contrat"></p>
        <p>copies originales, entre <input type="text" name="doc1"> et <input type="text" name="doc2">.</p>

        <div id="zoneDeTexteContainer">
         <!-- apparition des zone de texte -->
        </div>

        <button onclick="ajouterZoneDeTexte()">Ajouter une zone de texte</button>
        <!--- Lien pour la page javascipt de la premier zone d'ecriture -->
        <script src="zonetexte1.js" defer></script>
    </section>

    <section>
        <h2>1. NOM DU PARTENARIAT ET ACTIVITE</h2>
        <p>
            <h4>1.1 Nature des activités :</h4>
            Les partenaires cités ci-dessus donnent leur accord d'être considérés comme des partenaires commerciaux pour les fins suivantes:
            <input type="text" name="doc4">
        </p>

        <p>
            <h4>1.2 Nom :</h4>
            Les partenaires cités ci-dessus donnent leur accord pour que le partenariat commercial soit exercé sous le nom:
            <input type="text" name="doc5">
        </p>

        <p>
            <h4>1.3 Adresse officielle:</h4>
            Les partenaires cités ci-dessus donnent leur accord pour que le siège du partenariat commercial soit
            <input type="text" name="doc6">
        </p>
    </section>

    <section>
        <h2>2. TERMES</h2>
        <p>
        <h4>2.1 : Le partenariat commence le <input type="date" name="doc6"> et continue jusqu'à la fin de cet accord.</h4>
    </section>

    <section>
        <h2>3. CONTRIBUTION AU PARTENARIAT</h2>
        <p>
            <h4>3.1 La contribution de chaque partenaire au capital listée ci-dessous se compose des éléments suivants:</h4>
            <p>
            <input type="texte" name="doc7">
            <p>
            <div id="zoneDeTexteContainer2"> 
            <!-- apparition des zone de texte -->
            </div>
            <script src="zonetexte2.js" defer></script>
             <!-- Lien pour la page javascipt de la deuxieme zone d'ecriture -->
            <button onclick="ajouterZoneDeTexte2()">Ajouter une autre zone de texte</button>
        </p>
    </section>

    <section>
        <h2>4. REPARTITION DES BENEFICES ET DES PERTES</h2>
        <p>
            <h4>4.1 Les Partenaires se partageront les profits et les pertes du partenariat commercial de la manière suivante :</h4>
            <input type="texte" name="doc10"></h4> 
        </p>
    </section>

    <section>
        <h2>5. PARTENAIRES ADDITIONNELS</h2>
        <p>
            <h4>5.1 Aucune personne ne peut être ajoutée en tant que partenaire et aucune autre autre activité ne peut être menée par le partenariat sans le consentement écrit de tous les partenaires.</h4>
        </p>
    </section>
    
    <section>
        <h2>6.MODALITÉS BANCAIRES ET TERMES FINANCIERS</h2>
        <p>
            <h4>6.1 Les Partenaires doivent avoir un compte bancaire au nom du partenariat sur lequel les chèques doivent être signés par au moins des Partenaires :</h4>
            <input type="texte" name="doc11">
            <p>
            <h4> 6.2 Les Partenaires doivent tenir une comptabilité complète du partenariat et la rendre disponible à tous les Partenaires à tout moment.</h4>
        </p>
    </section>

    <section>
        <h2>7.GESTION DES ACTIVITÉS DE PARTENARIAT</h2>
        <p>
            <h4>7.1 Chaque partenaire peut prendre part dans la gestion du partenariat.</h4>
            <p>
            <h4>7.2 Tout désaccord qui intervient dans l'exploitation du partenariat, sera réglé par les partenaires détenant la majorité des parts du partenariat</h4>
        </p>
    </section>

    <section>
        <h2>8. DÉPART D'UN PARTENAIRE COMMERCIAL</h2>
        <p>
            <h4>8.1 Si un partenaire se retire du partenariat commercial pour une raison quelconque, y compris le décès, les autres partenaires peuvent continuer à exploiter le partenariat sous le même nom.</h4>
            <p>
            <h4>8.2 Le partenaire qui se retire est tenu de donner un préavis écrit d'au moins soixante (60) jours de son intention de se retirer et est tenu de vendre ses parts du partenariat commercial.</h4>
            <p>
            <h4>8.3 Aucun partenaire ne doit céder ses actions dans le partenariat commercial à une autre partie sans le consentement écrit des autres partenaires.</h4>
            <p>
            <h4>8.4 Le ou les partenaires restants paieront au partenaire qui se retire, ou au représentant légal du partenaire décédé ou handicapé, la valeur de ses parts dans le partenariat.</h4>
            <p>
            <h4>8.4 Le ou les partenaires restants paieront au partenaire qui se retire, ou au représentant légal du partenaire décédé ou handicapé, la valeur de ses parts dans le partenariat sont :</h4>
            - la somme de son capital.
            <p>
            - des emprunts impayés qui lui sont dus. 
            <p> 
            - sa quote-part des bénéfices nets cumulés non distribués dans son compte.
            <p>
            - son intérêt dans toute plus-value préalablement convenue de la valeur du partenariat par rapport à sa valeur comptable.
            <p> 
            <h4>Aucune valeur de bonne volonté ne doit être incluse dans la détermination de la valeur des parts du partenaire.</h4>
        </p>
    </section>

    <section>
        <h2>9. CLAUSE DE NON CONCURRENCE</h2>
        <p>
            <h4>9.1 Un partenaire qui se retire du partenariat ne doit pas s'engager directement ou indirectement dans une entreprise qui est ou serait en concurrence avec la nature des activités actuelles ou futures du partenariat pendant : </h4> 
            <input type="date" name="doc12"> au <input type="date" name="doc13">
    </section>

    <section>
        <h2>10. MODIFICATION DE L’ACCORD DE PARTENARIAT</h2>
        <p>
            <h4>10.1 Ce contrat de partenariat commercial ne peut être modifié sans le consentement écrit de tous les partenaires.</h4> 
        </p>
    </section>

    <section>
        <h2>11. DIVERS</h2>
        <p>
            <h4>11.1 Si une disposition ou une partie d'une disposition de la présente convention de partenariat commercial est non applicable pour une quelconque raison, elle sera dissociée sans affecter la validité du reste de la convention.</h4> 
            <p>
            <h4>11.2 Cet accord de partenariat commercial lie les partenaires commerciaux et pourra servir à leurs héritiers, exécuteurs testamentaires, administrateurs, représentants personnels, successeurs et ayants droit respectifs. </h4>
    </section>

    <section>
        <h2>12. JURIDICTION</h2>
        <p>
            12.1 Le présent contrat de partenariat commercial est régi par les lois de l’État de <input type="text" name="doc13">.
        </p>
        <p>
            Solennellement affirmé à <input type="text" name="doc13">
        </p>
        <p>
            Daté de ce jour <input type="date" name="doc13">
        </p>
        <p>
            Signé, validé et livré en présence de: 
        </p>
        <p>
        <div id="zoneDeTexteContainer3">
            <!-- apparition des zone de texte -->
        </div>
        <p>
        <button onclick="ajouterZoneDeTexte3()">Ajouter une zone de texte</button>
        <script src="zonetexte3.js" defer></script>
        <!--- Lien pour la page javascipt de la troisieme zone d'ecriture -->
        <p>
            Par moi: <input type="text" name="doc18">
        </p>
    </section> 

    <section>
        <!--- bonton de validation du formulaire -->
        <input type="submit" value="Enregistrer le contrat">
    </section>
</body>
</html>