// Initialise le compteur à 1
var compteurZonesDeTexte = 1;

// commande pour ajouter une nouvelle zone de texte
function ajouterZoneDeTexte() {
    
    var nouvelleZoneDeTexte = document.createElement("input");

    nouvelleZoneDeTexte.type = "text";

    nouvelleZoneDeTexte.className = "zoneDeTexte";

    // ajout d'un compteur 
    nouvelleZoneDeTexte.placeholder = "Zone de texte " + compteurZonesDeTexte;

    document.getElementById("zoneDeTexteContainer").appendChild(nouvelleZoneDeTexte);

    document.getElementById("zoneDeTexteContainer").appendChild(document.createElement("br"));

    compteurZonesDeTexte++;
}