<?php
include 'config.php';
session_start();

// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['user_id'])) {
    // Rediriger vers la page de connexion si l'utilisateur n'est pas connecté
    header('Location: login.php'); // Remplacez login.php par la page de connexion réelle
    exit();
}

// Récupérer l'ID de l'utilisateur connecté
$user_id = $_SESSION['user_id'];

// Sélectionner les formulaires de l'utilisateur connecté
$sql = "SELECT * FROM contrat_partenariat WHERE user_id = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("i", $user_id);
$stmt->execute();
$result = $stmt->get_result();

// Afficher la liste des formulaires
while ($row = $result->fetch_assoc()) {
    echo "ID du contrat : " . $row['id'] . "<br>";
    echo "Date du contrat : " . $row['debut_du_contrat'] . "<br>";
    // Ajoutez d'autres champs que vous souhaitez afficher
    echo "<a href='modifier_formulaire.php?id=" . $row['id'] . "'>Modifier</a> | ";
    echo "<a href='supprimer_formulaire.php?id=" . $row['id'] . "'>Supprimer</a><br><br>";
}

// Fermer la déclaration
$stmt->close();
?>
