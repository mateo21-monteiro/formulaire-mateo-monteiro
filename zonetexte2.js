// Fonction pour ajouter une nouvelle zone de texte avec un autre nom
function ajouterZoneDeTexte2() {
 
    var nouvelleZoneDeTexte2 = document.createElement("input");

    nouvelleZoneDeTexte2.type = "text";

    nouvelleZoneDeTexte2.className = "zoneDeTexte2";

    nouvelleZoneDeTexte2.placeholder = "Zone de texte2 " + compteurZonesDeTexte;

    document.getElementById("zoneDeTexteContainer2").appendChild(nouvelleZoneDeTexte2);

    document.getElementById("zoneDeTexteContainer2").appendChild(document.createElement("br"));

    compteurZonesDeTexte++;
}
