<!--- code enregistrement formulaire -->
<!--- Matéo Monteiro -->
<!--- BTS SIO 1 -->
<!--- TP classe pour Mr langloy -->   
<?php
include 'config.php';
session_start();

// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['user_id'])) {
    // Rediriger vers la page de connexion si l'utilisateur n'est pas connecté
    header('Location: login.php'); // Remplacez login.php par la page de connexion réelle
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $user_id = $_SESSION['user_id'];
    $debut_du_contrat = $_POST['debut_du_contrat'];
    $doc1 = $_POST['doc1'];
    $doc2 = $_POST['doc2'];
    $doc4 = $_POST['doc4'];
    $doc5 = $_POST['doc5'];
    $doc6 = $_POST['doc6'];
    $terme_debut = $_POST['terme_debut'];
    $doc7 = $_POST['doc7'];
    $doc10 = $_POST['doc10'];
    $doc11 = $_POST['doc11'];
    $doc12 = $_POST['doc12'];
    $doc13 = $_POST['doc13'];
    $doc18 = $_POST['doc18'];
    $date_signature = date('Y-m-d');

    // Préparation de la requête SQL
    $sql = "INSERT INTO contrat_partenariat (user_id, debut_du_contrat, doc1, doc2, doc4, doc5, doc6, terme_debut, doc7, doc10, doc11, doc12, doc13, doc18, date_signature) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    // Préparation de la déclaration pour l'exécution
    $stmt = $conn->prepare($sql);

    // Liaison des paramètres avec les valeurs
    $stmt->bind_param("issssssssssssss", $user_id, $debut_du_contrat, $doc1, $doc2, $doc4, $doc5, $doc6, $terme_debut, $doc7, $doc10, $doc11, $doc12, $doc13, $doc18, $date_signature);

    // Exécution de la requête
    if ($stmt->execute()) {
        echo "Le contrat a été ajouté avec succès!";
    } else {
        echo "Erreur lors de l'ajout du contrat : " . $stmt->error;
    }

    // Fermeture de la déclaration
    $stmt->close();
}
?>