<!--- code base de donnée -->
<!--- Matéo Monteiro -->
<!--- BTS SIO 1 -->
<!--- TP classe pour Mr langloy -->     
<?php
// Informations de connexion à la base de données
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'registration');

// Connexion à la base de données
$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

// Vérification de la connexion
if ($conn->connect_error) {
    die("Erreur de connexion à la base de données : " . $conn->connect_error);
}

// Configuration des caractères de la base de données
$conn->set_charset("utf8");
?>
